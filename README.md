# Automotive sample images

This is the git repository for the CentOS Automotive SIG sample images
working with osbuild and other tools. These images are considered 'proof
of concept' images for how parties can work with the CentOS Automotive SIG
repositories and tools.

## A note about git submodules

The sample-images git repository uses git submodules, which
means you have to do some extra work when using it:

When cloning the repo the first time, pass the "--recursive" option to also
clone the submodules:

```
$ git clone --recursive https://gitlab.com/CentOS/automotive/sample-images.git
```

If you have an existing git repository that doesn't have the submodule
checked out, then you can do it after the fact using:

```
$ git submodule update --init
```

When updating to the latest version of a branch using `git pull` you
must pass the `--recursive-submodules` option to also update the
submodule, or alternatively you can run `git submodule update` after
the pull.

For more details, see this [tutorial](https://www.vogella.com/tutorials/GitSubmodules/article.html).

## Further documentation

Documentation for making images are included in the
`osbuild-manifests/Makefile` using either `make` or `make list-targets`

Further information about this repository is kept up to date in the
Automotive SIG documentation website. For more information, visit:

* [Automotive SIG wiki page](https://wiki.centos.org/SpecialInterestGroup/Automotive)
* [Automotive SIG documentation](https://sigs.centos.org/automotive)


Other important websites:
* [Image Builder](https://www.osbuild.org/)
* [Image Builder Documentation](https://www.osbuild.org/guides/introduction.html)
  
  
